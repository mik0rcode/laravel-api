<?php


namespace App\Http\Repository\ItemRepository;


use App\Item;

class ItemRepository implements IItemRepository
{

    private $item;

    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    function getAll()
    {
        return $this->item->all();
    }

    function get(int $id)
    {
        return $this->item->find($id);
    }

    function create(array $attr)
    {
        return $this->item->create($attr);
    }

    function update(int $id, array $attr)
    {
        $item = $this->get($id);

        $item->update($attr);

        return $item;
    }

    function delete(int $id)
    {
        $item = $this->get($id);

        $item->delete();

        return $item;
    }
}