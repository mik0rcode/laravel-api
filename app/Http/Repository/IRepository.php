<?php


namespace App\Http\Repository;


interface IRepository
{
    function getAll();
    function get(int $id);
    function create(array $attr);
    function update(int $id, array $attr);
    function delete(int $id);
}