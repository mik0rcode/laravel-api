<?php

namespace App\Http\Controllers;

use App\Http\Service\ItemService\ItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{

    private $itemService;
    private $rules = [
        'name' => 'required',
        'description' => 'required',
        'price' => 'min:1'
    ];

    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->itemService->getAll());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        return response()->json($this->itemService->create($request->all()), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->itemService->get($id);

        if ($item == null) {
            return response()->json('Item not found', 404);
        }

        return response()->json($item);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        $item = $this->itemService->update($id, $request->all());

        if ($item == null) {
            return response()->json('Item not found', 404);
        }

        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->itemService->delete($id);

        if ($item == null) {
            return response()->json('Item doesn\'t exist.', 404);
        }

        return response()->json($item);
    }

}
